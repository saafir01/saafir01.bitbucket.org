## OL Introduction to Web Development for Java ##
### Unit 2 Web UI - Section 1: HTML ###
- [Lab 1 – Simple Page Lab Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/HTML/Lab1/simple.html)
- [Lab 2 – HTML Formatting Lab Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/HTML/Lab2/formattingDemoPage.html)
- [Lab 3 – Album Page Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/HTML/Lab3/albumPage.html)
- [Lab 4 – Contact Us Form Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/HTML/Lab4/contactUs.html)
- [Lab 5 - Restaurant Site v1 Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/HTML/Lab5/restaurantHome.html)

### Unit 2 Web UI - Section 2: CSS ###
- [Lab 1: Album Page v2 Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/CSS/Lab1/albumPage.html)
- [Lab 2: Restaurant Site v2 Assignment ](http://saafir01.bitbucket.org/Pre-Work/Certificates/CSS/Lab2/restaurantMenu.html)
- [Lab 3: Album Page v3 Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/CSS/Lab3/albumPage.html)
- [Lab 4: Restaurant Site v3 Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/CSS/Lab4/restaurantHome.html)
- [Lab 5: Album Page v4 Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/CSS/Lab5/albumPage.html)
- [Lab 6: Restaurant Site v4 Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/CSS/Lab6/restaurantHome.html)
- [Lab 7: Album Page v5 Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/CSS/Lab7/albumPage.html)
- [Lab 8: Restaurant Site v5 Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/CSS/Lab8/restaurantHome.html)
- [Lab 9: Restaurant Site v6 Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/CSS/Lab9/restaurantHome.html)
- [Calculation Assignment](http://saafir01.bitbucket.org/Pre-Work/Certificates/CSS/CalculationAssignment/boxModelCalculationsQuiz.txt)